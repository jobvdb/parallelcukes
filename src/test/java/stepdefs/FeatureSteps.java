package stepdefs;

import cucumber.TestContext;
import cucumber.api.java.en.Given;
import pages.HomePage;

public class FeatureSteps extends BaseSteps {

	private static final long SLEEP_TIME = 2000L;
	HomePage page;
	public FeatureSteps(TestContext ctx) {
		super(ctx);
		page = new HomePage(ctx);
	}

	@Given("I want to write a step with precondition \"(.+)\"")
	public void first(String word) throws Exception {
		getContext().getWebDriver().get("http://www.google.nl/search?q=" + word);
		page.searchFor("Hello World").verifyResult("Hello world (programma) - Wikipedia");
		getLOG().info(word);
	}

	@Given("some other precondition")
	public void second() throws Exception {
		Thread.sleep(SLEEP_TIME);
		getLOG().info("F1S2");
	}

	@Given("I complete action")
	public void third() throws Exception {
		getLOG().info("F1S2");
	}
}
