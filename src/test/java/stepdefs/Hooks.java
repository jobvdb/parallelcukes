package stepdefs;

import cucumber.TestContext;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks extends BaseSteps {

	public Hooks(TestContext ctx) {
		super(ctx);
	}

	@Before()
	public void setup() {
		System.out.println("SETUP");
	}

	@After()
	public void tearDown() {
		getContext().getWebDriver().quit();
		System.out.println("TEARDOWN");
	}
}