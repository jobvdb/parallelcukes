package stepdefs;

import java.util.logging.Logger;

import cucumber.TestContext;
import lombok.Getter;
import lombok.Setter;

public class BaseSteps {
	@Getter
	@Setter
	private TestContext context;
	
	@Getter
	private static final Logger LOG = Logger.getLogger("BaseSteps");
	
	public BaseSteps(TestContext ctx) {
		context = ctx;
	}
}
