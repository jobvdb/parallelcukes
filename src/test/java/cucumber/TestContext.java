package cucumber;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import lombok.Getter;

public class TestContext {
	
	@Getter	
	private WebDriver webDriver;

	public TestContext() {
		webDriver = new RemoteWebDriver(DesiredCapabilities.chrome());
	}
}
