package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.TestContext;

public class HomePage extends BasePage {
	
	@FindBy(name = "q")
	private WebElement searchBox;

	public HomePage(TestContext ctx) {
		super(ctx);
		PageFactory.initElements(getTestContext().getWebDriver(), this);
	}

	public ResultPage searchFor(String keyword) {
		searchBox.sendKeys(keyword);
		searchBox.submit();
		return new ResultPage(testContext);
	}
}
