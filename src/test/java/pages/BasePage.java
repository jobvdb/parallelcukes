package pages;

import cucumber.TestContext;
import lombok.Getter;

public class BasePage {
	@Getter
	TestContext testContext;
	
	protected BasePage(TestContext ctx) {
		this.testContext = ctx;
	}
}
