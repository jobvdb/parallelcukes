package pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.TestContext;

public class ResultPage extends BasePage {

	@FindBy(tagName = "h3")
	private List<WebElement> results;

	public ResultPage(TestContext ctx) {
		super(ctx);		
		PageFactory.initElements(getTestContext().getWebDriver(), this);
	}

	public ResultPage verifyResult(String expected) {
		for (WebElement result : results) {
			if(result.getText().equals(expected)) {
				break;
			}
		}
		return this;
	}	
}
