# Parallel Cukes

A complete setup to start using Cucumber with parallel feature execution, Dependency Injection (picocontainer) and the Selenium Page Object model.

## Prerequisites 

These tests require a Selenium Server to be available hosted at localhost. To see the parallel execution, make sure your Selenium Server is configured with multiple Chrome instances.

Test will be run in a random order.

## Run the tests

To run, execute `mvn clean test`.

## Report

A cucumber report is generated at `target\cucumber`.